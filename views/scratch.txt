//this text file contains modified content pre version

//Sanzaal info block
.col-lg-4.col-sm-6.wow.fadeInLeft.delay-05s
            h2 Sanzaal
            h6 in nutshell
            .service-list
              .service-list-col1
                i.fa-users
              .service-list-col2
                h3 Networking Hub
                p  
            .service-list
              .service-list-col1
                i.fa-arrow-up
              .service-list-col2
                h3 Skill development
                p  
            .service-list
              .service-list-col1
                i.fa-asterisk
              .service-list-col2
                h3 Promotion of Nepalese Culture
                p 
            .service-list
              .service-list-col1
                i.fa-book
              .service-list-col2
                h3 Education project for Nepal
                p
                
//upcoming events
if(newEvents.length > 0)
        section.main-section.newEvent(style="padding-bottom: 0px;") 
            .container
              .row
                #newEvent(class="col-lg-12") 
                    .col-lg-4.col-sm-6.wow.fadeInLeft.delay-05s
                        h2 Upcoming Event !!
                        h6 #{newEvents[0].myEvents[0].eventName}
                        .service-list 
                          .service-list-col2
                            h3 Description
                            p  #{newEvents[0].myEvents[0].description}  
                        .service-list 
                          .service-list-col2
                            h3 Price
                            p  #{newEvents[0].myEvents[0].price}
                        .service-list 
                          .service-list-col2
                            h3 Venue
                            a(style="text-decoration:underline;" href="http://www.reittiopas.fi/?from_in=&to_in=#{newEvents[0].myEvents[0].place}&when=now&timetype=departure" target="ext")  #{newEvents[0].myEvents[0].place}
                            | |
                            a( href="https://www.google.fi/maps/place/#{newEvents[0].myEvents[0].place}" target="ext")
                              i.fa.fa-map-marker
                        .service-list 
                          .service-list-col2
                            h3 Date
                            p   #{newEvents[0].myEvents[0].eventDate.toISOString().toString().slice(0,10)}
                        .service-list 
                          .service-list-col2
                            h3 Time
                            p  #{newEvents[0].myEvents[0].eventTime}  
                    .col-lg-8.col-sm-6
                        #myCarousel.carousel.slide
                            // Indicators 
                            .carousel-inner
                             each item, index in newEvents[0].myEvents[0].photo
                              if(index == 0)
                                .item.active
                                    img.img-responsive(src='#{item}') 
                              else
                                .item
                                    img.img-responsive(src='#{item}') 
                            // Controls
                            a.left.carousel-control(href='#myCarousel', data-slide='prev')
                              span.icon-prev
                            a.right.carousel-control(href='#myCarousel', data-slide='next')
                              span.icon-next      
                        .alert.alert-info(role='alert')
                            .col-lg-4.col-sm-4.text-center
                                a(style="text-decoration:underline;" href="#{newEvents[0].myEvents[0].facebookEvent}" target="ext") Facebook >> 
                            .col-lg-4.col-sm-4.text-center
                                a(style="text-decoration:underline;" href="/selectedEvent/#{newEvents[0].eventType}/#{newEvents[0].myEvents[0].eventName}"  ) Details >>
                            .col-lg-4.col-sm-4.text-center
                                a(href="#" id="eventBookButton" data-target="#bookEvent" data-toggle="modal" style="text-decoration:underline;") #{newEvents[0].tagWord} >>
                            br
                        .alert.alert-success.emailSent(style="display:none" role="alert")
                          strong Thank you!
                          | We have received your booking.
                    .col-sm-3
                        
                    .clearfix
                    
                #bookEvent.modal.fade(tabindex='-1', role='dialog', aria-labelledby='myModalLabel')
                    .modal-dialog(role='document')
                      .modal-content
                        .modal-header
                          button.close(type='button', data-dismiss='modal', aria-label='Close')
                            span(aria-hidden='true') x
                        
                          h4#myModalLabel.modal-title Book for #{newEvents[0].myEvents[0].eventName}
                        .modal-body  
                         form.form-horizontal.bookEventForm1
                          .form-group
                            label.col-sm-2.control-label(for='inputEmail3') Name
                            .col-sm-9
                              input#eventDescription.form-control(type='text' name="bookerName" required)
                          .form-group
                            label.col-sm-2.control-label(for='inputPassword3') Email
                            .col-sm-9
                              input#inputPassword3.form-control(type='email', name="bookerEmail" placeholder="sanzaalry@gmail.com" required)
                          .form-group
                            label.col-sm-2.control-label(for='inputPassword3') Mobile
                            .col-sm-9
                              input#inputPassword3.form-control(type='text', name="bookerMobile" placeholder="+358445359441"  )
                          .form-group
                            label.col-sm-2.control-label(for="ticketPrice") Price: #{newEvents[0].myEvents[0].price}e
                            label.col-sm-2.control-label(for='inputPassword3') Total 
                            .col-sm-2
                              input#inputPassword3.form-control(type='number', name="ticketCount" placeholder="1" max=10 min=1 required)
                            .col-sm-6   
                          .form-group
                            label.col-sm-2.control-label(for='inputPassword3') 
                            .col-sm-9
                              input#inputPassword3.form-control(type='text', name="optionalWord"  placeholder="#{newEvents[0].optionalWord}"  )
                          if(parseInt(newEvents[0].myEvents[0].isTicket) > 0)
                            .form-group
                              label.col-sm-9.control-label(for="sendToAddress")  Do you want the ticket to be sent via postal service?
                              .col-sm-offset-1.col-sm-3
                                label.radio-inline
                                  input(type='radio', name='sendToAddress' value="1" required)
                                  | Yes
                                label.radio-inline
                                  input(type='radio', name='sendToAddress' value="0")
                                  | No
                            .form-group
                              .myAddress
                                label.col-sm-2.control-label(for='inputPassword3') Address
                                .col-sm-9
                                  input#inputPassword3.form-control(type='text', name="address" placeholder="Tyynenmerenkatu 7B 43 00220 Helsinki"  )
                          .form-group
                            
                            .col-sm-offset-2.col-sm-10
                              #example2
                              br
                              button.btn.btn-default(type='submit') Book
                              input(type="hidden" name="eventId" value="#{newEvents[0].myEvents[0].id}")
                              input(type="hidden" name="bankReference" value="#{newEvents[0].myEvents[0].bankReference}")
                              input(type="hidden" name="eventName" value="#{newEvents[0].myEvents[0].eventName}")
//slider previeous
figure.col-lg-12.col-sm-12.text-right.wow.fadeInUp.delay-02s
            #slider 
                .viewer
                  .reel
                    .slide
                      a.link(href='https://scontent-arn2-1.xx.fbcdn.net/hphotos-xpt1/t31.0-8/11058056_450914611744305_4766997574745722280_o.jpg') Full story ...
                      img(src='/images/changa.jpg',  alt='')
                    .slide
                      //a.link(href='http://nodethirtythree.com/#slidertron-slide-2') Full story ...
                      img(src='/images/npevt.jpg', alt=''  )
                    .slide
                      //a.link(href='http://nodethirtythree.com/#slidertron-slide-3') Full story ...
                      img(src='/images/nuk.jpg', alt=''  )
                    .slide
                      img(src='/images/cycle.jpg')
                    .slide
                      img(src='/images/sabin_c.jpg')
                    .slide
                      img(src='/images/sabin_c1.jpg')
                    .slide
                      img(src='/images/changa_1.jpg')