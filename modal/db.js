var mongoose = require('mongoose'),
 	bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10,
    dbURI = 'mongodb://localhost/sanzaal';
 
mongoose.connect(dbURI,function(err){
    if (err) {
        //code
        console.log("cannot make connection ");
    }
    }); 
mongoose.connection.on('connected',function(){
   console.log('Mongoose connected to' + dbURI);
});    
mongoose.connection.on('error',function(err){
  console.log('Mongoose connection error: '+  err);
});

mongoose.connection.on('disconnected', function(){
	console.log('Mongoose disconnected');
});
 
process.on('SIGINT',function(){
	mongoose.connection.close(function(){
		console.log('Mongoose disconnected through app termination');
		process.exit(0);
	})
});


// Event Types
var eventTypeSchema = new mongoose.Schema({
    
    eventName:String,
    eventCreated:Date
});





/*****************************************************
 ********************* Event Detail ******************
 *****************************************************
 */
var event = new mongoose.Schema({
        eventName:String,
        description:String,
        eventDate: Date,
        eventTime: String,
        place: String,
        photo: Array,
        video: Array,
	price:String,
	bankReference:String,
	isTicket: Number, 
	facebookEvent:String,
	publish:Number
    });

/* *****************************************************
 ****************** Events Model ********************** 
 *******************************************************
 */
var eventSchema = new mongoose.Schema({
               eventType:String,
               myEvents:[event],
               createdAt:Date,
	       tagWord:String, // eg. Register Now, Book now
	       optionalWord:String // eg. xBox or PS4 , What time you would like to visit us? etc
    
            });

mongoose.model('Events',eventSchema);



mongoose.model('EventType',eventTypeSchema);




/* *************************************
 * *************** Message Model ************
 * ************************************
 */
 var messageSchema = new mongoose.Schema({
    
        senderName: String,
        senderEmail: String,
        subject: String,
        messageContent: String,
        receivedDate: Date
    });
 mongoose.model('Message',messageSchema);
 
 
 
 /* ***************************************
  * ******************** USER Model *******
  * ******************** ****************
  */
var userSchema = new mongoose.Schema({
   
  name: {type: String, required: true }, 
  password: {type: String,   required : true  } ,
  createdDate:Date
   
   
});

userSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
 

// Build the User model
mongoose.model( 'User', userSchema );


// session information

var sessionSchema = new mongoose.Schema({
	sessionId:{type:String},
	sessionUser:{type:String},
	sessionIp:{type:String},
	sessionInTime:{type:Date},
	sessionCountry:String,
	sessionLatitude:String,
	sessionLongitude:String,
	sessionISP:String
	
});

mongoose.model('Session',sessionSchema);

// intrusion information

var intrusionSchema = new mongoose.Schema({

	sessionUser:String,
	sessionPassword:String,
	attackAt:Date,
	sessionIp:{type:String},
	sessionCountry:String,
	sessionLatitude:String,
	sessionLongitude:String,
	sessionISP:String
	
	
});

mongoose.model('Intrusion',intrusionSchema);


// member model

var memberSchema = new mongoose.Schema({
    name:String,
    email:String,
    mobile:String,
    memberType:String,
    designation:String,
    createdDate:Date,
    status:Number,
    facebook:String,
    fbImage:String,
    position:Number
});

mongoose.model('Member',memberSchema);



//meeting invitation model

var meetingInvitationSchema = new mongoose.Schema({
    meetingDate:Date,
    meetingTime:String,
    meetingPlace:String,
    meetingSubject:String,
    meetingAgenda: String,
    notifiedTo:[],
    createdOn:Date
    
});
mongoose.model('MeetingInvitation', meetingInvitationSchema);


// Booker model
var bookerSchema = new mongoose.Schema({
    name:String,
    email:String,
    mobile:String,
    address:String,
    ticket:Number,
    eventName:String,
    eventId:String,
    uniqueId:String,
    createdAt:Date,
    bankReference:String,
    payersBank:String,
    paidAmount:Number,
    isPaid:Number,
    isChecked:Number,
    time:String,
    ticketNumber:String,
    optionalWord:String
    
});

mongoose.model('Booker', bookerSchema);
