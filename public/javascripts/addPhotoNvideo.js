$(document).ready(function(){
    
    
    // create event type (category of events)
    $('.eventCategoryForm').on('submit',function(e){
        e.preventDefault();
        
          
        $.ajax({
            url:"/eventAdmin",
            data:$('.eventCategoryForm').serialize(),
            dataType:'json',
            method:'post',
            success:function(data){
                if (data) {
                    location.reload();
                }
            },
            error:function(err){
                alert(err);
            }
        });
        
    });
    
    // edit name of the event type (category event)
    $('.editEventTypeForm').on('submit',function(e){
        e.preventDefault();
        
          
        $.ajax({
            url:"/editEventType",
            data:$('.editEventTypeForm').serialize(),
            dataType:'json',
            method:'post',
            success:function(data){
                if (data) {
                    location.reload();
                }
            },
            error:function(err){
                alert(err);
            }
        });
        
    });
    
    $('#addMedia').on('submit',function(event){
         
        event.preventDefault();
        console.log($('#addMedia').serialize());
        var url =   '/eventAdmin/myEvents/'+data;
        
        $.ajax({
            url:url,
            dataType:'json',
            type:'post',
            data: $('#addMedia').serialize(),
            success:function(data){
                console.log(data);
                $('#addMedia')[0].reset();
                $('#myModal').modal('hide');
            },
            error:function(err){
                if (err) {
                    //code
                    alert(err);
                }
            }
        });

    });
    
    $('#createEventDescription').on('submit',function(event){
        event.preventDefault();
      
        var para = CKEDITOR.instances.eventDescription.getData();
	 	$('#createEventDescription textarea[name=eventDescription]').val(para);
        var url =   '/eventAdmin/myEventDescription/'+data;
        
            //code
            $.ajax({
            url:url,
            dataType:'json',
            type:'post',
            data: $('#createEventDescription').serialize(),
            success:function(data){
                console.log('Description\n');
                console.log(data);
                $('#createEventDescription')[0].reset();
                location.reload();
            },
            error:function(err){
                if (err) {
                    //code
                    alert(err);
                }
            }
        });
       
    });
    
    
    //sending query or feedback to server
    
    $('#sendMessage').on('submit',function(event){
        event.preventDefault();
      
        
        var url =   '/queryMsg' ;
        
            //code
            $.ajax({
            url:url,
            dataType:'json',
            type:'post',
            data: $('#sendMessage').serialize(),
            success:function(data){
               
                console.log(data);
                $('#sendQuery').blur();
                $('#sendMessage')[0].reset();
                if (data) {
                    //code
                    var msg = "Dear "  + data.name + ", \n" +  "Thank you for contacting us. We will get back to you as soon as possible.";
                    alert(msg);
                    
                }
                
            },
            error:function(err){
                if (err) {
                    //code
                    console.log(err);
                    alert(err);
                }
            }
        });
       
    });
    
    // adding new member to the system
    $('.addMemberForm').on('submit',function(event){
        event.preventDefault();
         $('.addNewMember').blur();
         console.log($('.addMemberForm').serialize());
      
        var url =   '/addMember' ;
        
            //code
            $.ajax({
            url:url,
            dataType:'json',
            type:'post',
            data: $('.addMemberForm').serialize(),
            success:function(data){
               
                console.log(data);
               
                $('.addMemberForm')[0].reset();
                 $('#addMember').modal('hide');
                if (data) {
                    //code
                    
                    location.reload();
                }
                
            },
            error:function(err){
                if (err) {
                    //code
                    console.log(err);
                    alert(err);
                }
            }
        });
       
    });
    
    
    // editing existing member to the system
    $('.editMemberForm').on('submit',function(event){
        event.preventDefault();
        
         console.log($('.addMemberForm').serialize());
      
        var url =   '/editMember' ;
        
            //code
            $.ajax({
            url:url,
            dataType:'json',
            type:'post',
            data: $('.editMemberForm').serialize(),
            success:function(data){
               
                console.log(data);
               
                $('.editMemberForm')[0].reset();
                 $('#editMember').modal('hide');
                if (data) {
                    //code
                    
                    location.reload();
                }
                
            },
            error:function(err){
                if (err) {
                    //code
                    console.log(err);
                    alert(err);
                }
            }
        });
       
    });
    
    $('.addMeetingInvitiation').on('submit',function(event){
        event.preventDefault();
        console.log($('.addMeetingInvitiation').serialize());
        var url =   '/meetingNotification' ;
        
            //code
            $.ajax({
            url:url,
            dataType:'json',
            type:'post',
            data: $('.addMeetingInvitiation').serialize(),
            success:function(data){
               
                console.log(data);
               
                $('.addMeetingInvitiation')[0].reset();
                
                if (data) {
                    //code
                    
                    location.reload();
                }
                
            },
            error:function(err){
                if (err) {
                    //code
                    console.log(err);
                    alert(err);
                }
            }
        });
       
    });
    
    
    
  
    
    
    //submitting the booking update isPaid yes or some other attributes changed
    $('.editBookingForm').on('submit',function(e){
        e.preventDefault();
        $.ajax({
            url:'/updateBooking',
            method:'post',
            dataType:'json',
            data:$('.editBookingForm').serialize(),
            success:function(data){
                //location.reload();
                console.log('update booking is paid ?')
                console.log(data);
                bookerData = data;
                 $('#particularEventBookingUpdate tbody').remove();
                  
                  var tbody = '<tbody>';
                  
                   for(var i=0;i<data.length;i++){
                    var address = "";
                    var ticket = "";
                    var optional = "";
                    if (typeof data[i].address == "undefined") {
                        address = "";
                    }else{
                        address = data[i].address;
                    }
                    if (typeof data[i].ticketNumber == "undefined") {
                        ticket = "";
                    }else{
                        ticket = data[i].ticketNumber;
                    }
                    if (typeof data[i].optionalWord == "undefined") {
                        optional = "";
                    }else{
                        optional = data[i].optionalWord;
                    }
                    tbody += '<tr><td>' + (i+1) + '</td>' +
                            '<td>'  + data[i].name +'</td>'+
                             '<td>'+data[i].mobile+'</td>'+
                             '<td>'+address+'</td>'+
                             '<td>'+data[i].uniqueId+'</td>'+
                             '<td>'+ ticket+'</td>'+
                             '<td>'+ data[i].optionalWord +'</td>' +
                             '<td>'+ '<a href="#" data-target="#bookingEvent" data-toggle="modal" id="'+i+'" class="editBookingButtonClass">Edit</a>'+'</td>'+
                             '</tr>';
                   }
                   tbody += '</tbody>'
                    $('#particularEventBookingUpdate ').append(tbody);
                    
                    
                    $('#bookingEvent').modal('hide');
            },
            error:function(err){
                alert(err);
            }
        });
    });
    
    
    
    // deactivating member
    $('.deactivateMemberForm').on('submit',function(e){
        e.preventDefault();
        
        $.ajax({
            url:'/deactivateMember',
            data:$('.deactivateMemberForm').serialize(),
            dataType:'json',
            method:'post',
            success:function(data){
                location.reload();
            },
            error:function(err){
                alert(err);
            }
        });
    });
    
    
    // activating member
    $('.activateMemberForm').on('submit',function(e){
        e.preventDefault();
        
        $.ajax({
            url:'/activateMember',
            data:$('.activateMemberForm').serialize(),
            dataType:'json',
            method:'post',
            success:function(data){
                location.reload();
            },
            error:function(err){
                alert(err);
            }
        });
    });
    
    
    
    
    $('a.addEventsAnchorClass').click(function(e){
         $('.checkBookingDivClass').hide('fast');
          $('.checkBookingAttendesDivClass').hide('fast');
         $('.addEventDescriptionDivClass').show('fast');
    });
    $('a.checkBookingAnchorClass').click(function(e){
        $('.checkBookingDivClass').show('fast');
        $('.addEventDescriptionDivClass').hide('fast');
         $('.checkBookingAttendesDivClass').hide('fast');
    });
    
    $('a.checkAttendesAnchorClass').click(function(){
        $('.checkBookingAttendesDivClass').show('fast');
        $('.addEventDescriptionDivClass').hide('fast');
        $('.checkBookingDivClass').hide('fast');
    });
    
    
   
    
    
    //updating attendee checking status
    $('.editAttendesBookingForm').on('submit',function(e){
        e.preventDefault();
        $.ajax({
            url:'/updateAttendeStatus',
            dataType:'json',
            data:$('.editAttendesBookingForm').serialize(),
            method:'post',
            success:function(data){
                console.log(data);
                // update the table
                bookerData = data;
                $('#particularEventAttendes tbody').remove();
                  
                  var tbody = '<tbody>';
                  
                   for(var i=0;i<data.length;i++){
                    var isChecked = (data[i].isChecked>0)?'Yes':'No';
                    tbody += '<tr><td>' + (i+1) + '</td>' + '<td>'  + data[i].name +'</td>'+
                             '<td>'+data[i].mobile+'</td>'+ '<td>'+ data[i].ticketNumber+'</td>'+
                             '<td>'+ isChecked+'</td>'+
                             '<td>'+ '<a href="#" data-target="#bookingAttendesEvent" data-toggle="modal" id="'+i+'" class="editBookingAttendesButtonClass">Edit</a>'+'</td>'+
                             '</tr>';
                   }
                   tbody += '</tbody>'
                    $('#particularEventAttendes ').append(tbody);
                    
                    
                    $('#bookingAttendesEvent').modal('hide');
            },
            error:function(){
                alert('something went wrong.');
            }
        });
    });
    
    
    // change event name
    $('.editEventForm').on('submit',function(e){
        e.preventDefault();
        $.ajax({
            url:'/changeEventName',
            dataType:'json',
            data:$('.editEventForm').serialize(),
            method:'post',
            success:function(data){
                if (data.REPLY === "OK") {
                    window.location.href = '/eventAdmin/myEvents/'+ $('.editEventForm input[name=newEventName]').val();
                    //location.reload();
                }else{
                    alert('some thing went wrong. Please contact to Binit ');
                }
            },
            error:function(err){
                alert('some thing went wrong.');
            }
            
        });
    });
    
    
    $('.editPhotoLinkForm').on('submit',function(e){
       e.preventDefault();
       $.ajax({
            url:'/editPhotoLink',
            dataType:'json',
            data:$('.editPhotoLinkForm').serialize(),
            method:'post',
            success:function(data){
                location.reload();
            },
            error:function(err){
                alert('something went wrong');
            }
        });
    });
});



  // change event name
$(document).on('click','.editEventName', function(e){
        
         
        $('.editEventForm input[name=oldEventName]').val(data.trim());
        
        
        
});

  // updating booking --- is booking amount Paid  ??
$(document).on('click','.editBookingButtonClass', function(e){
        var id = $(this).attr('id');
        console.log('clicked'+id);
        var myData = bookerData[id];
        $('.editBookingForm input[name=bookerName]').val(myData.name)
        $('.editBookingForm input[name=bookerMobile]').val(myData.mobile)
        $('.editBookingForm input[name=address]').val(myData.address)
        $('.editBookingForm input[name=bookingCode]').val(myData.uniqueId)
        if (myData.isPaid > 0) {
            //code
            //alert(myData.isPaid);
            $('.editBookingForm select[name=isPaid]').val('1');
        }else{
            $('.editBookingForm select[name=isPaid]').val('0');
        }
        
        $('.editBookingForm input[name=bookerId]').val(myData._id);
        $('.editBookingForm input[name=isChecked]').val(myData.isChecked);
        $('.editBookingForm input[name=eventId]').val(myData.eventId);
        
});

 $(document).on('click','a.editBookingAttendesButtonClass',function(e){
        var id = $(this).attr('id');
        console.log('clicked'+id);
        var myData = bookerData[id];
        $('.editAttendesBookingForm input[name=bookerName]').val(myData.name)
        $('.editAttendesBookingForm input[name=bookerMobile]').val(myData.mobile)
        
        if (myData.isChecked > 0) {
            //code
            $('.editAttendesBookingForm select[name=isChecked]').val('1');
        }else{
            $('.editAttendesBookingForm select[name=isChecked]').val('0');
        }
        
        $('.editAttendesBookingForm input[name=bookerId]').val(myData._id);
        $('.editAttendesBookingForm input[name=eventId]').val(myData.eventId);
        
        
    });
 $(document).on('click','a.editPhotoButtonClass',function(e){
    var id = $(this).attr('id');
    console.log('clicked'+id);
    console.log(dataItem);
     $('.editPhotoLinkForm input[name=oldPhotoLink]').val(dataItem[0].myEvents[0].photo[id])         //
    $('.editPhotoLinkForm input[name=newPhotoLinkId]').val(id)
     $('.editPhotoLinkForm input[name=myEventsId]').val(dataItem[0].myEvents[0]._id)
        
});
    