var submit = false;
$(document).ready(function(){
     $('#myCarousel').carousel({ interval: 3000, cycle: true });
     
     //slider
     /*
     $('#slider').slidertron({
          viewerSelector: '.viewer',
          reelSelector: '.viewer .reel',
          slidesSelector: '.viewer .reel .slide',
          advanceDelay: 3000,
          speed: 'slow',
          navPreviousSelector: '.previous-button',
          navNextSelector: '.next-button',
          slideLinkSelector: '.link'
      });
     */
     
     
     $('.newEvents-btn').click(function(){
          var index = $(this).attr('id');
          var selectedData = dataItem[index];
          var title = "Participate in " + selectedData.myEvents[0].eventName;
          $('.modalTitle ').text(title);
          console.log("id ="+selectedData.myEvents[0]._id);
          $('.bookEventForm1 input[name=eventId]').val(selectedData.myEvents[0]._id)
     });
      
     
      $('.bookEventForm .myAddress').hide('fast');
      $('.emailSent').hide('fast');
      console.log(dataItem);
     $('.bookEventForm1').on('submit',function(e){
        e.preventDefault();
         e.stopImmediatePropagation();
      var check = check_if_capcha_is_filled();
      if (check) {
            $.ajax({
            url:'/bookit',
            data:$('.bookEventForm1').serialize(),
            dataType:'json',
            type:'post',
            success:function(data){
                  if (data.REPLY === "NOK") {
                        alert('something went wrong try again later.');
                  }else{
                       $('.emailSent').show('fast');
                   console.log('this is called');
                    $('#bookEvent').modal('hide');
                    $('.bookEventForm1')[0].reset();
                    setTimeout(function(){
                        $('.emailSent').hide('fast'); 
                    },5000);  
                  }
                   
                  
            },
            error:function(err){
                console.log(err);
                
            }
        });  
      }
        
     }); 
     
     $('.bookEventForm input[name=sendToAddress]').on('change',function(e){
         var value = $(this).val();
        if (value == 1) {
            $('.bookEventForm .myAddress').show('fast');
             $('.bookEventForm input[name=address]').prop('required',true);
        }else{
            $('.bookEventForm .myAddress').hide('fast');
             $('.bookEventForm input[name=address]').prop('required',false);
        }
 
    });
     
     
     //flush the form when modal is closed
     $('#bookEvent').on('hidden.bs.modal',function(e){
         $('.bookEventForm1')[0].reset();
         $('.bookEventForm1 .myAddress').hide('fast');
    });

     console.log(contactItem);
     
     //sending query to server
      $('.sendMessage').on('submit',function(event){
            event.preventDefault();
             event.stopImmediatePropagation();
            
            var url =  '/queryMsg' ;
            console.log($('.sendMessage').serialize());
           var check = check_if_capcha_is_filled();
          
           if (check) { 
            $.ajax({
                  url:url,
                  dataType:'json',
                  type:'post',
                  data: $('.sendMessage').serialize(),
                  success:function(data){
                     
                      console.log(data);
                      $('.input-btn').blur();
                      $('.sendMessage')[0].reset();
                      if (data) {
                          //code
                          
                          if (data.REPLY=="NOK") {
                              alert('re-validate captcha');
                              
                          }else{
                               var msg = "Dear "  + data.name + ", \n" +  "Thank you for contacting us. We will get back to you as soon as possible.";
                              alert(msg);
                          }
                         grecaptcha.reset();
                          
                      }
                      
                  },
                  error:function(err){
                      if (err) {
                          //code
                          console.log(err);
                          alert(err.toString());
                      }
                  }
            });
           }
            
            
       
    });
      
      
      
      
       aload();
       // alert(selectData[0].myEvents[0].eventName);
        //alert('bac');
        var url = window.location.href;
        if(url.indexOf("contact")> -1){
           //window.location = window.location.href.replace(/([?].*)/, "");
           //console.log(url1);
           
            $('html, body').stop().animate({
              scrollTop:$('#contact').offset().top-60
              }, 1500,'easeInOutExpo');
              
             // window.location = url1;
              
        }else if(url.indexOf("team")> -1){
           window.location = window.location.replace(/([?].*)/, "")
          $('html, body').stop().animate({
              scrollTop:$('#team').offset().top-60
              }, 1500,'easeInOutExpo');
        }
        
      
});
 function capcha_filled () {
    submit = true;
}
function capcha_expired(){
        submit = false;
        grecaptcha.reset();
      }
function check_if_capcha_is_filled(e){
  if(submit) return true;
  
  alert('Fill in the captcha')
}
  function aload(t){"use strict";t=t||window.document.querySelectorAll("[data-aload]"),void 0===t.length&&(t=[t]);var a,e=0,r=t.length;for(e;r>e;e+=1)a=t[e],a["LINK"!==a.tagName?"src":"href"]=a.getAttribute("data-aload"),a.removeAttribute("data-aload");return t}