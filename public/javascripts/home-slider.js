$(document).ready(function(){
    $( '#home-slider' ).sliderPro({
        width: '100%',
        height:500,
        arrows: true,
        buttons: false,
        waitForLayers: true,
        fade: true,
        autoplay: true,
        autoScaleLayers: false,
        thumbnailPointer:true,
        thumbnailWidth:'24.55%',
        thumbnailHeight:100,
        breakpoints:{
            1024:{height:450},
            668:{height:350,buttons: true},
            480:{height:200,buttons: true}
        }
        
        });
    
})