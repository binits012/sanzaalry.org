jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
     $('.noEntry').hide('fast');
     $('.entry').hide('fast');
     $('.noPaid').hide('fast');
    var imageUrl = dataItem[0].myEvents[0].photo[0];
    console.log(dataItem);
    $.backstretch(imageUrl);
    
    /*
        Form validation
    */
    $('.downloadEticket-form input[type="text"]').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.downloadEticket-form').on('submit', function(e) {
    	e.preventDefault();
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			 
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
                        
                        $.ajax({
                            url:'/sendETicket',
                            method:'post',
                            dataType:'json',
                            data:$('.downloadEticket-form').serialize(),
                            success:function(data){
                                console.log(data);
                                if(data.REPLY == "NOTPAID"){ 
                                    $('.noPaid').show('fast');
                                    $('.noEntry').hide('fast');
                                    $('.entry').hide('fast');
                                }else if (data.REPLY == "NODATA") {
                                     
                                     $('.noEntry').show('fast');
                                     $('.entry').hide('fast');
                                     $('.noPaid').hide('fast');
                                }else{
                                     $('.noEntry').hide('fast');
                                     $('.entry').show('fast');
                                     $('.noPaid').hide('fast');
                                    
                                }
                            },
                            error:function(err){
                                alert('something went wrong');
                            }
            
                            
                        });
    		}
    	});
    	
    });
    
    
});