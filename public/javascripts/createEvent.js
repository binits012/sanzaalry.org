 
$(document).ready(function(){
    /*
    console.log("dataItem");
     console.log(dataItem.myEvents);
     var myData = [];
     for(var i=0;i<dataItem.length;i++){
        console.log(dataItem[i].myEvents);
        var thisData = {
            "sn":i+1,
            "name":dataItem[i].myEvents[0].eventName,
            "createdAt":1,
            "action":2
        };
        myData.push(thisData);
     }
   $('#eventsTable').bootstrapTable("append",myData);
   */
    $('#createEvent').on('submit',function(event){
        event.preventDefault();
         
        var eventName = $('#eventName').val();
        
        var url = "/createEvent/"+data;
       
        if (eventName) {
           
            $.ajax({
                url:url,
                type:'post',
                dataType:'json',
                data:$('#createEvent').serialize(),
                success:function(data){
                console.log( data);
                $('#creatEventSubmit').blur();
                  $('#eventsTable tbody').remove();
                  location.reload();
                  /*
                  var tbody = '<tbody>';
                  
                   for(var i=0;i<data.length;i++){
                      console.log(data[i].myEvents);
                      console.log("event name %s",data[i].myEvents[0].eventName);
                      var anchor = "/eventAdmin/myEvents/"+data[i].myEvents[0].eventName;
                    tbody += '<tr><td>' + (i+1) + '</td>' + '<td>' + '<a href="'+anchor+'" >' + data[i].myEvents[0].eventName +'</a>' +'</td>'+
                             '<td>'+data[i].createdAt.slice(0,10)+'</td>'+ '<td><a href="#"  class="editEventButtonClass" data-target="#editEvents" data-toggle="modal" id="'+i+'">Edit</a>'+
                             '<a href="#" class="eventDeleteButtonClass" data-target="#deleteEvents" data-toggle="modal" id="'+i+'"> | Delete </a>'+'</td>'+'</tr>';
                   }
                   tbody += '</tbody>'
                    $('#eventsTable ').append(tbody);
                    
                    $('#createEvent')[0].reset();
                  */
                },
                error:function(err){
                    if (err) {
                        console.log("error is" + err);
                        alert(err.status);
                    }
                }
                });
        }
    });
    
    $('#editEvents').on('show.bs.modal',function(e){
      //  console.log(dataItem);
       
        
    });
    
     $('#editEvents').on('hidden.bs.modal',function(e){
         
         $('.editEventButtonClass').blur();   
    });
     
     
    //submiting form for editing events
    $('.editEventForm').on('submit',function(e){
         e.preventDefault();
         var para = CKEDITOR.instances.eventDescription.getData();
	 	$('.editEventForm textarea[name=eventDescription]').val(para);
        console.log(para);
         $.ajax({
            url:'/eventAdmin/editEvent',
            data:$('.editEventForm').serialize(),
            dataType:'json',
            type:'post',
            success:function(data){
                if (data.length > 0) {
                    //code
                    $('#editEvents').modal('hide');
                    location.reload();
                }else{
                    alert('something went wrong. ');
                    
                    
                }
            },
            error:function(err){
                alert(err);
            }
         });
         
    });
    
    
    
    
});


//
//submitting the delete request to server
    $(document).on('submit','.deleteEventForm',function(e){
        e.preventDefault();
        $.ajax({
            url:"/deleteEvent",
            dataType:'json',
            data:$('.deleteEventForm').serialize(),
            method:'post',
            success:function(data){
                location.reload();
            },
            error:function(err){
                alert('something went wrong');
            }
        });
    });

// deleting event
    $(document).on('click','a.eventDeleteButtonClass',function(){
        var id = $(this).attr('id');
        var myData = dataItem[id];
        console.log(myData._id);
        $('.deleteEventForm input[name=eventName]').val(myData.myEvents[0].eventName);
        $('.deleteEventForm input[name=eventId]').val(myData._id);
    });
    
    $(document).on('click','a.editEventButtonClass',function(){
       
       $('.editEventDiv').show();  
       var id = $(this).attr('id');
        
        var myData = dataItem[id];
         //$('#editEventDiv #myModalLabel').text(myData.myEvents[0].eventName);

         if (myData.myEvents[0].description) {
            console.log('description ' + myData.myEvents[0].description);
            CKEDITOR.instances.eventDescription.setData( myData.myEvents[0].description  );
            $('#editEventDiv .editEventForm #eventDescription').text(myData.myEvents[0].description);
         }else{
             $('#editEventDiv .editEventForm #eventDescription').text("");
         }
         console.log("eventDescription = " + myData.myEvents[0]._id);
          $('.editEventForm input[name=eventId]').val(myData.myEvents[0]._id);
          
         if (myData.myEvents[0].place) {
            $('.editEventForm input[name=venue]').val(myData.myEvents[0].place);
         }else{
            $('.editEventForm input[name=venue]').val("");
         }
         if (myData.myEvents[0].eventDate) {
            
             $('.editEventForm input[name=eventDate]').val(myData.myEvents[0].eventDate.slice(0,10));
         }else{
            $('.editEventForm input[name=eventDate]').val("");
         }
        
         $('.editEventForm input[name=eventTime]' ).val(myData.myEvents[0].eventTime);
	 $('.editEventForm input[name=price]' ).val(myData.myEvents[0].price);
	 $('.editEventForm input[name=image]' ).val(myData.myEvents[0].photo[0]);
	 $('.editEventForm input[name=bankReference]').val(myData.myEvents[0].bankReference);
	 /*
         $('#editEvents .editEventForm input[name=eventId]').val(myData.myEvents[0]._id);
         $('#editEvents .editEventForm input[name=price]').val(myData.myEvents[0].price);
         $('#editEvents .editEventForm input[name=fbEvent]').val(myData.myEvents[0].facebookEvent);
         $('#editEvents .editEventForm input[name=bankReference]').val(myData.myEvents[0].bankReference);
         $('#editEvents .editEventForm input[name=tagWord]').val(myData.tagWord);
         $('#editEvents .editEventForm input[name=optionalWord]').val(myData.optionalWord);
         var isTicket = myData.myEvents[0].isTicket;
         console.log(isTicket);
         if (isTicket == undefined || isTicket == "0") {
             $('#editEvents .editEventForm #ticketNo').prop('checked',true);
              $('#editEvents .editEventForm #ticketYes').prop('checked',false);
         }else  {
            //code
            $('#editEvents .editEventForm #ticketYes').prop('checked',true);
            $('#editEvents .editEventForm #ticketNo').prop('checked',false);
         }*/
         //alert(myData.myEvents[0].isTicket);
    });