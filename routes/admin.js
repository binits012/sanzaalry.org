/* GET admin home page. */
var mongoose = require('mongoose');
var EventType = mongoose.model('EventType');
var Events = mongoose.model('Events');
var User = mongoose.model( 'User' ); 
var Session = mongoose.model('Session');
var Intrusion = mongoose.model('Intrusion');
var satelize = require('satelize');
var Message = mongoose.model('Message');
var Member = mongoose.model('Member');
var nodemailer = require("nodemailer");
var Booker = mongoose.model('Booker');
var MeetingInvitation = mongoose.model('MeetingInvitation');
var SendMail = require('../routes/sendMail'); 
var transport = nodemailer.createTransport({
service: 'Gmail',
auth: {
    user: 'sanzaalry@gmail.com',
    pass: 'Since2014'
}
});
var clearSession = function(session, callback){
  session.destroy();
  callback();
};
exports.logOut = function(req,res){
  clearSession(req.session, function () {
    res.redirect('/admin');
  });

  };

function attackInSystem(sessionUser,sessionPassword,sessionIp){
 
 	satelize.satelize({ip:sessionIp}, function(err, geoData) {
 	
 		if(!err){
 			var data = JSON.parse(geoData);
		    var isp = data.isp;
		    var longitude = data.longitude;
		    var latitude= data.latitude;
		    var country = data.country;
		    
		    Intrusion.create({
		    	sessionUser:sessionUser,
				sessionPassword:sessionPassword,
				attackAt:new Date(),
				sessionIp:sessionIp,
				sessionCountry:country,
				sessionLatitude:latitude,
				sessionLongitude:longitude,
				sessionISP:isp
		    
		    },function(err,data){
		    	if(err){
		    		console.log('something went wrong');
		    	}else{
		    		console.log('intrusion saved');
		    	}
		    });
 		}
 	});
 
  
 } 
exports.admin = function(req, res){
  
  if (req.session.name) {
    //code
     res.render('admin',{title:'Sanzaal Ry',user:req.session.name.name});
  }else{
    res.render('login', { title: 'Admin Sanzaal ry'});
  }
  
  
};
exports.login = function(req,res){
  var name = req.body.username;
  var password = req.body.password;
   
	console.log("********* password ****'" + password);
	var ip =  req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	
 
        if(name && password){
	User.findOne({'name':name},function(err,user){
	
		if(err){
		
			res.send('opppssss something went wrong');
		
		}
		else{
			console.log(user);
			if(user != null){
		   user.comparePassword(password,function(error,isMatch){
          if(error){
          	
          }else{
              if (isMatch) {
                console.log("password match");
                 req.session.name = { "name" : user.name  };
                 
		    	console.log("**********USER LOGIN *****************");
		    	
		    	
		    	console.log('ip=',ip);
				/*
		    	satelize.satelize({ip:ip}, function(err, geoData) {
		    		if(!err){
						console.log("this is it" +  geoData);
		    			//console.log(JSON.parse(geoData));
						
		    			var data = JSON.parse(geoData);
		    			var isp = data.isp;
		    			var longitude = data.longitude;
		    			var latitude= data.latitude;
		    			var country = data.country;
		    			console.log(isp+longitude,latitude,country);
		    			
		    			Session.create({
		    				sessionId:req.sessionID,
							sessionUser:req.body.userName,
							sessionIp:ip,
							sessionInTime:new Date(),
							sessionCountry:country,
							sessionLatitude:latitude,
							sessionLongitude:longitude,
							sessionISP:isp
		    			},function(err,data){
		    				if(!err){
		    					console.log('session saved');
		    				}else{
		    					console.log('something went wrong');
		    				}
		    			
		    			});
		    		}
				}); // jsonp output for this ip
                */
                res.render('admin',{title:'Sanzaal Ry',user:name});
		    	 
 		     	
              }
              else{
              	//attackInSystem(req.body.userName,password,ip);
                res.send("User name or passowrd not valid. Go Back and try again !!");
              }
          }  
        });
		   }else{
		   
		 //   attackInSystem(req.body.userName,password,ip);
		   	
		   	res.send('sorry we do not recognize you. Please go back and use your username and password');
		   }
		  
		}
	});
 	}
 	else{
	    res.send("User name or passowrd not valid. please try again later");
           
    } 
  
  
}


exports.createNewUserView = function(req,res){
    
    res.render('creatUser',{'title':'User create'});
} 
exports.createNewUser = function(req,res){
	var name = req.body.username;
	var password = req.body.password;
	 
	//console.log("********** companyId ******" + companyId);
	 
	User.create({ name: name, 
				password: password,
                createdDate: new Date
				}, function( err, user ){
    		var qstring = '';
    		if(err){
      			console.log(err);
      			if(err.code===11000){
        			qstring = 'exists=true';
      			} else if (err.name === "ValidationError") {
        			for (var input in err.errors) {
          				qstring += input + '=invalid&';
         				 console.log(err.errors[input].message);
        			}
      			} 
              		 
      			res.send( 'error in creating new users');
   	  	}else{
      			console.log("User created and saved: " + user.name + user);
     	  
     		   //req.session.userName = { "name" : user.name  };
     		    res.send('user created'); 
    		}
  	}); 
        //req.session.user = { "name" : user.name "_id": user._id };
         // req.session.loggedIn = true;
	

}
 
exports.eventAdmin = function(req, res){
  if (req.session.name) {
    //code
    EventType.find({},function(err,data){
    if(!err){
      res.render('eventsAdmin', { title: 'Admin Sanzaal ry',data:data,user:req.session.name.name });
     
    }
    });
  }else{
    res.redirect('/admin');
   
  }
};
/* post request */
exports.eventAdminPost = function(req,res){
  
   if (req.session.name) {
    console.log('This is called postAdmin');
    var eventName = req.body.eventName;
    if (eventName != null) {
        //code
        EventType.create({
            eventName:eventName,
            eventCreated: new Date()
        },function(error,data){
            if (!error) {
                //code
                res.send(data);
            }else{
                console.log(error);
            }
            });
    }
   }else{
    res.redirect('/admin');
      
   }
 
}
/* edit the event type category */

exports.editEventType = function(req, res){
    
    if (req.session.name) {
	var eventTypeName = req.body.eventTypeName;
	var eventTypeId = req.body.eventTypeId;
	var newEventTypeName = req.body.newEventTypeName;
	
	EventType.findOneAndUpdate({'_id':eventTypeId},{$set:{'eventName':newEventTypeName}},function(err, data){
	    if(!err){
		Events.update({'eventType':eventTypeName},{'eventType':newEventTypeName},{multi:true},function(err,data){
		    if (!err) {
			console.log(data);
			res.send(data);
		    }
		});
	    }    
	});
	
	
	
    }else{
	res.redirect('/admin');
    }
}


/* get the event types event */
exports.eventAdminEvents = function(req,res){
  
  if (req.session.name) {
  var eventName = req.params.id;
  console.log('Event inside of Event Type '+ eventName);
  if (eventName) {
    //code
    console.log(eventName);
     Events.find({'eventType':eventName},function(err,doc){
              if (!err) {
                //code
               // res.send(doc);
               console.log(doc);
                 res.render('event',{title:eventName,data:eventName,dataItem:doc,user:req.session.name.name});
              }
    });
   
  }
  }else{
     res.redirect('/admin');
  }
}

exports.createAdminEvents = function(req,res){
  
  if (req.session.name) {
    //code
      console.log('new event created');
    var eventName = req.body.eventName.trim();
    var eventNameFromURL = req.params.id.trim();
    //var tagWord = req.body.tagWord;
  //  var optionalWord = req.body.optionalWord;
    console.log(eventNameFromURL + eventName);
    if (eventName) {
      
      //get the event album of selected
       
       EventType.find({"eventName":eventNameFromURL},function(err,data){
      if(!err){
        var myId = data[0].eventName ;
        console.log(myId);
        
        var event = new Events({eventType:myId,createdAt:new Date});
        
        //create event now
        
        event.myEvents.push({eventName:eventName /*, tagWord:tagWord, optionalWord:optionalWord*/});
        event.save(function(err){
            if (!err) {
              //code
               Events.find({'eventType':eventNameFromURL},function(err,doc){
                if (!err) {
                  //code
                  res.send(doc);
                }
                });
            }
          });
        
        
         
       
      }
      }); 
      
     
       
    }
    
  }else{
     res.redirect('/admin');
  }
  
  
  
  
}


exports.myParticularEvent = function(req,res){
  
   if (req.session.name) {
      var eventName = req.params.id.trim();
      
  console.log('Particular event called' + eventName);
  var eventName1 = eventName.replace(/(\')/gm,"\'");
  console.log('Particular event called123' + eventName1);
   Events.find()
   .where('myEvents.eventName').equals(eventName1)
   .exec(function(err,data){
    if (!err) {
      //code
      //console.log(data[0].myEvents[0]._id);
      var eId = data[0].myEvents[0]._id;
       Booker.find({'eventId':eId,'eventName':eventName1},function(err, bookerData){
	if (!err) {
	    //code
	     res.render('myEvent',{title:eventName,data:eventName, dataItem:data,user:req.session.name.name, bookerData:bookerData});
	}
       });
     
     
    }
    });
   }else{
     res.redirect('/admin');
   }
   
  
}

exports.myParticularEventPost = function(req,res){
  
  if (req.session.name) {
    console.log('this is called');
   var eventName = req.params.id;
   var eventName1 = eventName.replace(/(\')/gm,"\'");
  console.log('Particular event called123' + eventName1);
  console.log('Particular event called' + eventName);
   var dataValue = req.body.addMedia;
   dataValue =dataValue.filter(function(e){ return e.replace(/(\r\n|\n|\r)/gm,"")});
   console.log(dataValue);
    
   Events.update({'myEvents.eventName':eventName},{$pushAll: { 'myEvents.$.photo': dataValue }}).exec().then(function(err,data){
       Events.find({'myEvents.eventName':eventName},function(err,doc){
              if (!err) {
                //code
                res.send(doc);
              }});
    }); 
  }else{
    res.redirect('/admin');
  }
   
   
   
}


exports.myParticularEventPostDescription = function(req,res){
   if (req.session.name) {
    //code
     var eventName = req.params.id;
    var eventName1 = eventName.replace(/(\')/gm,"\'");
  console.log('Particular event called123 = ' + eventName1);
   var dataValue = req.body.eventDescription;
   var date = req.body.eventDate;
   var time = req.body.eventTime;
   var venue = req.body.eventVenue;
   var price = req.body.price;
   var fb = req.body.fbEvent;
   var bankReference = req.body.bankReference;
   var isTicket = req.body.isTicket;
   var tagWord = req.body.tagWord;
   var optionalWord = req.body.optionalWord;
    console.log(dataValue + date + time + venue);
     
    
    Events.findOne({ 'myEvents.eventName': eventName }, function (err, doc){
     doc.myEvents[0].description = dataValue;
     doc.myEvents[0].eventDate = date;
     doc.myEvents[0].eventTime = time;
     doc.myEvents[0].place = venue ;
     doc.myEvents[0].price = price;
     doc.myEvents[0].facebookEvent = null;
     doc.myEvents[0].bankReference = bankReference;
     doc.myEvents[0].isTicket = null;
     doc.tagWord = null;
     doc.optionalWord = null;
     console.log(doc);
      
      
      doc.save(function(err){
        if (err) {
          //code
	   console.log("err");
          console.log(err);
        }else{
	     console.log("saved");
          res.send(doc);
        }
        })
    });
  }else{
      res.redirect('/admin');
  }
 
    
}



//feedback listing
exports.feedback = function(req,res){
  if (req.session.name) {
    //code
    Message.find({})
    .sort({receivedDate:-1})
    .exec(function(err,data){
      if (!err) {
        //code
        res.render('feedback',{title:"Feedback",user:req.session.name.name,data:data});
      }
      });
    
    
  }else{
    res.redirect('/admin');
  }
  
}

// memberview render
exports.member = function(req, res){
	if (req.session.name) {
      //code
	  Member.find({'status':1},{},function(err,data){
		if (!err) {
		Member.find({'status':0},{},function(err,deActivatedData){
		    if (!err) {
		      console.log(data);
		      res.render('member',{title:'Sanzaal Member', user:req.session.name.name,data:data, dData:deActivatedData});
		    }
		});
	    }
	  });
    }else{
		res.redirect('/admin');
	}
}

// adding new member
exports.addMember = function(req,res){
	if (req.session.name) {
      //code
	  /*
	   *  name:String,
    email:String,
    mobile:String, 
    memberType:String,
    designation:String,
     createdDate:Date,
    status:Number
    */
	   
	  var name = req.body.memberName;
	  var email = req.body.memberEmail;
	  var phone = req.body.phone;
	  var designation = req.body.designation;
	  var memberType = req.body.memberType;
	  var facebook = req.body.facebookLink;
	  var memberImg = req.body.memberImgLink;
	  var position = req.body.position;
	  console.log(name + email + phone + designation + memberType);
	  if (typeof email === 'undefined') {
	    email = null;
	  }
	  if (typeof phone === 'undefined') {
	    phone = null;
	  }
	  if (typeof memberType === 'undefined') {
	    memberType = null;
	  }
	  if (typeof facebook === 'undefined') {
	    facebook = null;
	  }
	   
	  Member.create({
			name:name,
			email:email,
			mobile:phone,
			memberType:memberType,
			designation:designation,
			createdDate:new Date,
			status:1,
			facebook:facebook,
			fbImage:memberImg,
			position:position
			
		},function(err,data){
			if (!err) {
              //code
			  res.send(data);
            }else{
				
				res.send('something went wrong. Please go back and try again');
			}
		}); 
	   
    }else{
		
		res.redirect('/admin');
	}
}

// edit member
exports.editMember = function(req,res){
	if (req.session.name) {
      //code
	   var name = req.body.memberName;
	  var email = req.body.memberEmail;
	  var phone = req.body.phone;
	  var designation = req.body.designation;
	  var memberType = req.body.memberType;
	  var facebook = req.body.facebookLink;
	  var memberId = req.body.memberId;
	  var memberImg = req.body.memberImgLink;
	  console.log(name + email + phone + designation + memberType + memberImg);
	
	  Member.findOneAndUpdate({'_id':memberId},{$set:{'email':email,'mobile':phone,'name':name,'designation':
							  designation, 'memberType':memberType, 'facebook':facebook,'fbImage':memberImg}},{new:'true'},function(err,data){
		if (!err) {
          //code
		  console.log(data);
		  res.send(data);
        }else{
			res.send('something went wrong. Please try later!!');
		}
	  });
	   
	   
    }else{
		res.redirect('/admin');
	}
}

exports.meetingNotification = function(req,res){
	if (req.session.name) {
      //code
	  Member.find({},{},function(err,data){
		if (!err) {
          //code
		  console.log(data);
		  MeetingInvitation.find({}).
		  sort({createdOn:-1}).
		  exec(function(err,notificationData){
			if (!err) {
              //code
			  res.render('meetingNotification',{title:'Sanzaal Meeting Notification', user:req.session.name.name,data:data, notificationData:notificationData});
            }else{
			  res.send('Something went terribly wrong. Contact the site administrator');	
			}
		  });
		  
        }
	  });
    }else{
		res.redirect('/admin');
	}
}
exports.meetingNotificationPost = function(req,res){
	
	if (req.session.name) {
      //code
	  var meetingDate = req.body.meetingDate;
	  var meetingTime = req.body.meetingTime;
	  var meetingPlace = req.body.meetingPlace;
	  var meetingSubject = req.body.meetingSubject;
	  var meetingAgenda = req.body.meetingAgenda;
	  var meetingEmails = req.body.memberList;
	  console.log(meetingAgenda + '\n'+meetingAgenda.split('\n').toString().replace(/(',|\r')/gm,""));
	  var agendaWithLineBreak = meetingAgenda.split('\n') ;
	  
	  console.log(meetingEmails);
	  
	  
	  var upperContent = "Dear Members,\n\n\n Notice is hereby given to all the members that a meeting of Sanzaal Ry will be held on "+ meetingDate +" at " + meetingTime + " in" + meetingPlace + ",  to the following businesses. \n"
	  var message = { 
                        // sender info
                        from: 'sanzaalry@gmail.com', 
                        // Comma separated list of recipients
                        to: meetingEmails, 
                        // Subject of the message
                        subject: 'NOTICE FOR THE MEETING OF SANZAALRY.', // 
                        text:upperContent +'\n\n'+  meetingAgenda  + '\n\n\nBest Regards\n Sanzaal Ry'
                    };
	   
	    
	  transport.sendMail(message, function(error){
								if(!error){
									console.log("Email has been sent"); 
									 
								}else{
									console.log(error );
								
								}
							});
	   
	  
	  
	  // now we are persistig the data into the system
	  /*
	   *  meetingDate:Date,
    meetingTime:String,
    meetingPlace:String,
    meetingSubject:String,
    meetingAgenda: String,
    notifiedTo:[],
    createdOn:Date
    */
	  var meetingInvitation = new MeetingInvitation({
		meetingDate:new Date(meetingDate),
		meetingTime:meetingTime,
		meetingPlace: meetingPlace,
		meetingSubject: meetingSubject,
		meetingAgenda:meetingAgenda,
		notifiedTo:meetingEmails,
		createdOn: new Date
	 });
	  
	  meetingInvitation.save(function(err, data){
		if (!err) {
          //code
		  res.send( data);
        }else{
			
		  res.send('Something went wrong. Please report to the administrator');	
		}
		
	  });
	  
    }else{
		res.redirect('/admin');
	}
}





exports.editEvent = function(req,res){
    
    if (req.session.name) {
    var eventDescription = req.body.eventDescription;
    var venue = req.body.venue;
    var eventDate = req.body.eventDate;
    var eventTime = req.body.eventTime;
    var eventId = req.body.eventId;
    var price = req.body.price;
    var fb = req.body.fbEvent;
    var  bankReference = req.body.bankReference;
    var isTicket = req.body.isTicket;
    var tagWord = req.body.tagWord;
    var optionalWord = req.body.optionalWord;
    var publish = req.body.publish;
    //console.log(eventDescription +  eventId);
    console.log('Event id =' + eventId);
    var image = req.body.image;

    
    if (eventId) {
	 
	Events.findOneAndUpdate({'myEvents._id':eventId},{$set: {
		    'myEvents.$.description':eventDescription,
		    'myEvents.$.eventDate':eventDate,
		    'myEvents.$.eventTime':eventTime,
		    'myEvents.$.place':venue,
		    'myEvents.$.price':price,
		    'myEvents.$.photo':image,
		    'myEvents.$.bankReference':bankReference,
		    'myEvents.$.publish':publish
		      }},function(err,data){
			if (err) {
			    //code
			    console.log(err);
			}else{
			     
			    console.log(data);
			     Events.find({'myEvents._id':eventId},function(err,doc){
				if (!err) {
				  //code
				  console.log('updated');
				  console.log(doc);
				  res.send(doc);
				}else{
				 console.log(err);
				}
			    });
			}
			
			}) ;
	    /*
	    Events.find({'myEvents._id':eventId},function(err,doc){
		   if (!err) {
		     //code
		     console.log('updated');
		     console.log(doc);
		     res.send(doc);
		   }else{
		    console.log(err);
		   }
		   });*/
	 
    }
    }else{
	res.redirect('/admin');
    }
};



function randomFixedInteger(length) {
  console.log('length' + length)
     var d = new Date().getTime(); 
    var uuid = 'xxxxx-xxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*11)%11 | 0;
        d = Math.floor(d/11);
        return (c=='x' ? r : (r&0x3|0x8)).toString(11);
    });
    return uuid;
}

function checkUnique(randomFixedNumber,callback){
   
    Booker.findOne({'uniqueId':randomFixedNumber },function(err,data){
	    if (!err) {
		//code
		if (data == null) {
		    
		    console.log('there is nothing');
		    console.log( randomFixedNumber); 
		    callback(1);
		   
		}else{
		    callback(0);
		    console.log('there is something');
		    console.log(data);
		}
	    }
	});
}
exports.updateBooking = function(req,res){
    
    if (req.session.name) {
    var name = req.body.bookerName.trim();
    var mobile = req.body.bookerMobile.trim();
    var address = req.body.address.trim();
    var bookingCode = req.body.bookingCode.trim();
    var isPaid = req.body.isPaid.trim();
    var bookerId =req.body.bookerId.trim();
    var isChecked = req.body.isChecked.trim();
    var eId = req.body.eventId.trim();
    
    if (bookerId) {
	
	
	
	
	if (isPaid === '0') {
	    Booker.findOneAndUpdate({'_id':bookerId},{$set: {
			'mobile':mobile,
			'address':address,
			'isPaid': isPaid
			  }},function(err,data){
			    if (!err) {
				console.log('booker updated -> NO'); 
				Booker.find({'eventId':eId},{},function(err, bookerData){
				    if (!err) {
					//code
					res.send(bookerData);  
				    }
				});
			    }
	    });  
	     
	     
	     
	}else{ 
	    var randomFixedNumber= randomFixedInteger(11);
	    console.log(randomFixedNumber);
		
	    var isUnique = function(unique){
		console.log('is unique ? =' + unique);
		if (isChecked == 1) {
		    Booker.findOneAndUpdate({'_id':bookerId},{$set: {
			'mobile':mobile,
			'address':address,
			'isPaid': isPaid
			
			  }},function(err,data){
			    if (!err) {
				Booker.find({'eventId':eId},{},function(err, bookerData){
				    if (!err) {
					//code
					console.log('booker updated -> YES');
					res.send(bookerData);  
				    }
				});
			    }
			}); 
		}else{
		    if (unique == 1 &&  isChecked == 0) {
		    Booker.findOneAndUpdate({'_id':bookerId},{$set: {
			'mobile':mobile,
			'address':address,
			'isPaid': isPaid,
			'ticketNumber':randomFixedNumber
			  }},function(err,data){
			    if (!err) {
				Booker.find({'eventId':eId},{},function(err, bookerData){
				    if (!err) {
					//code
					console.log(' new ticket for booker is added -> YES');
					res.send(bookerData);
					
					if (data.isTicket == 1 ) { 
					    var sendMail = new SendMail(name,data.eventName,"","","",data.uniqueId,data.email,"","");
					    //name,eventName,totalTicket,bankReference,ticketNumber,uniquNumberArray,email, eventDate, eventTime
					    sendMail.sendMail("E-Ticket ready for","eTicketLink");
					}else{
					   var sendMail = new SendMail(name,data.eventName,"","","",data.uniqueId,data.email,"","");
					    //name,eventName,totalTicket,bankReference,ticketNumber,uniquNumberArray,email, eventDate, eventTime
					    sendMail.sendMail("About","registration"); 
					}
					
				    }
				});
			    }
			}); 
		    }else{
			 
			    
			randomFixedNumber= randomFixedInteger(11);
			console.log('you need to generate new random number' + randomFixedNumber ); 
			checkUnique(randomFixedNumber,isUnique);
		 
			
		    }
		}
    		
	    }
	    checkUnique(randomFixedNumber,isUnique);
	    
	}
	
	
	
	
    }
    }else{
	res.redirect('/admin');
    }
}


exports.deactivateMember = function(req,res){
    if (req.session.name) {
	var name = req.body.memberName;
	var id = req.body.memberId;
	console.log(name + id);
	
	Member.findOneAndUpdate({'_id':id},{$set:{'status':0}},function(err, data){
	    if (!err) {
		res.send({'REPLY':'OK'});
	    }else{
		res.send({'REPLY':'NOK'});
	    }
	});
	
    }
    else{
	res.redirect('/admin');
    }
}

exports.activateMember = function(req,res){
    if (req.session.name) {
	var name = req.body.memberName;
	var id = req.body.memberId;
	console.log(name + id);
	
	Member.findOneAndUpdate({'_id':id},{$set:{'status':1}},function(err, data){
	    if (!err) {
		res.send({'REPLY':'OK'});
	    }else{
		res.send({'REPLY':'NOK'});
	    }
	});
     }
      else{
	res.redirect('/admin');
    }
}


exports.deleteEvent = function(req,res){
   if(req.session.name){
	var eventName = req.body.eventName;
	var eventId = req.body.eventId;
     
	Events.findOneAndRemove({'_id':eventId},{},function(err,data){
	   if(!err){
		console.log(data);
		res.send(data);
	    } 
	});
    }else{
	res.redirect('/admin');	
    } 
}


exports.updateAttendeStatus = function(req,res){
    if(req.session.name){
	var bookerName = req.body.bookerName.trim();
	var bookerId = req.body.bookerId.trim();
	var eId = req.body.eventId.trim();
	var isChecked = req.body.isChecked.trim();
	Booker.findOneAndUpdate({'_id':bookerId},{$set: {
		    'isChecked':isChecked 
		      }},function(err,data){
			if (!err) {
			    Booker.find({'eventId':eId},{},function(err, bookerData){
				if (!err) {
				    //code
				    res.send(bookerData);  
				}
			    });
			}
	}); 
	 
    }else{
	res.redirect('/admin');	
    } 
}


exports.changeEventName = function(req, res){
    if(req.session.name){
	var oldEventName = req.body.oldEventName.trim();
	var newEventName = req.body.newEventName.trim();
	newEventName = newEventName.replace(/(\')/gm,"\'");
	console.log(oldEventName);
	console.log(newEventName);
	 
	 
	 
	Events.findOne({ 'myEvents.eventName': oldEventName }, function (err, doc){
	    doc.myEvents[0].eventName = newEventName;
	   
	     
	     
	     doc.save(function(err){
	       if (err) {
		 //code
		 console.log(err);
	       }else{
		console.log(doc);
		 Booker.update({'eventName':oldEventName},{'eventName':newEventName},{multi:true},function(err,data){
			console.log(doc);
			if (!err) {
			    res.send({'REPLY':'OK'});
			}else{
			    res.send({'REPLY':'NOK'});
			}
		});
	       }
	       });
       }); 
	    
	 
    }else{
	res.redirect('/admin');	
    } 
}



exports.editPhotoLink = function(req, res){
     if(req.session.name){
	var oldPhotoLink = req.body.oldPhotoLink.trim();
	var newPhotoLink = req.body.newPhotoLink.trim();
	console.log(oldPhotoLink + newPhotoLink);
	var myEventsId = req.body.myEventsId.trim();
	
	Events.findOne({ 'myEvents._id': myEventsId }, function (err, doc){
	   // console.log(doc.myEvents);
	    console.log(doc.myEvents[0].photo);
	    var tempData = doc.myEvents[0].photo;
	     
	    for(var i=0;i< tempData.length;i++){
		console.log(tempData[i]);
		//console.log(index);
		
		if (tempData[i] === oldPhotoLink) {
		    console.log('photo found at =' + i);
		    //doc.myEvents[0].photo[i] = newPhotoLink;
		    doc.myEvents[0].photo.set(i,newPhotoLink);
		    break;
		}
		 
		
	    }
	    doc.save(function(err,data){
		if (!err) {
		    console.log(data.myEvents[0].photo);
		   res.send({'REPLY':'OK'});
		}
	    });
	    
	    
	     
	     
	}); 
	    
	 
    }else{
	res.redirect('/admin');	
    } 
}