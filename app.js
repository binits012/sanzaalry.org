var express = require('express');
var db = require('./modal/db');
var http = require('http');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var routes = require('./routes');
var users = require('./routes/user');
var admin = require('./routes/admin'); 
var app = express();

// view engine setup
app.set('port', process.env.PORT || 8082);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(express.cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret:"salpa",
  resave: false,rolling:true,
  saveUninitialized: false, cookie: { expires: new Date(Date.now() + 60 * 15000), 
  maxAge: 60*15000 }
}));
app.use(app.router);

app.get('/', routes.index);
app.get('/users', users.list);
app.get('/event/:id',routes.myEvent);
app.get('/contact',routes.contact);
app.get('/selectedEvent/:eventName/:eventYear',routes.selectedEvent);
app.post('/queryMsg',routes.queryMsg);
app.post('/bookit',routes.bookit);
app.get('/sendETicket',routes.downloadETicket);
app.post('/sendETicket',routes.generateETicket);



// URI for ADMIN
app.post('/admin',admin.login);
app.get('/admin',admin.admin);
//for creating admin user
app.get('/createNewUser',admin.createNewUserView);
app.post('/createNewUser', admin.createNewUser);
app.get('/eventAdmin',admin.eventAdmin);
app.post('/eventAdmin',admin.eventAdminPost);
app.post('/editEventType', admin.editEventType);
//get event type eg. changa chet
app.get('/eventAdmin/:id',admin.eventAdminEvents);
app.post('/createEvent/:id',admin.createAdminEvents);
//get event eg. changa chet 2014
app.get('/eventAdmin/myEvents/:id',admin.myParticularEvent)
app.post('/eventAdmin/myEvents/:id',admin.myParticularEventPost);
app.post('/eventAdmin/myEventDescription/:id',admin.myParticularEventPostDescription);
app.post('/eventAdmin/editEvent', admin.editEvent);
app.post('/updateBooking', admin.updateBooking);
app.post('/updateAttendeStatus', admin.updateAttendeStatus);
app.post('/changeEventName',admin.changeEventName);
app.post('/deleteEvent', admin.deleteEvent);
app.post('/editPhotoLink', admin.editPhotoLink);


app.get('/feedback',admin.feedback);
app.get('/member',admin.member);
app.post('/addMember',admin.addMember);
app.post('/editMember',admin.editMember);
app.post('/deactivateMember',admin.deactivateMember);
app.post('/activateMember',admin.activateMember);
app.get('/meetingNotification',admin.meetingNotification);
app.post('/meetingNotification',admin.meetingNotificationPost);
 
app.get('/logOut',admin.logOut);
/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
/*
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
*/

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//app.set('port', process.env.PORT || 8080);
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
module.exports = app;
